<?php
	## If you use composer uncomment line 4 and comment
	## line 7 to 13 
	# require_once(__ROOT__ . "/vendor/autoload.php");

	## Else :
	spl_autoload_register(function ($name) {
		$path = __ROOT__ . "/";
		if($name === "Core\\CoreManager") {
			$path .= "CoreManager.php";
		} else {
			$path .= $name . ".php";
		}
		$path = str_replace("\\", "/", $path);
		require_once($path);
	});
