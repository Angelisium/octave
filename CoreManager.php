<?php
	namespace Core;

	class CoreManager {
		protected $ContentType = "text/html";
		protected $response = [
			'code' => 200,
			'desc' => 'OK'
		];

		public function dispatch(...$args) {
			$RequestMethod = $_SERVER['REQUEST_METHOD'];
			$content = "";
			if(method_exists($this, $RequestMethod)) {
				$content = $this->$RequestMethod(...$args);
			} else {
				$this->response['code'] = 404;
				$this->response['desc'] = "Not Found";
				$content = "<html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL was not found on this server.</p></body></html>";
			}

			header($_SERVER['SERVER_PROTOCOL'] . " " . implode(" ", $this->response));
			header("Content-type: {$this->ContentType}; charset=UTF-8");
			echo $content;
			die();
		}
	}