# Soon

The objective of the project is to provide an robust MVC architecture but the Model and View sections are at the developer's expense (nevertheless, the project must provide examples with home made/composer use to facilitate the creation of the Model and View parts).
To see more, please going to [Introduction](https://gitlab.com/Angelisium/octave/-/wikis/Introduction).

## Sample config :
nginx.conf
```nginx.conf
server {
    listen      80;
    server_name 127.0.0.6;
    root        /www/public;
    index       index.php;

    location / {
        try_files     $uri @php;
        location ~* ^(.*)\.php$ {
            try_files $uri.php @php;
        }
    }
    location @php {
        try_files     $uri.php "${uri}index.php" /index.php =404;

        fastcgi_pass  php:9000;
        fastcgi_param SCRIPT_FILENAME
                      $document_root$fastcgi_script_name;
        fastcgi_index index.php;
        include       fastcgi_params;

        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_intercept_errors on;
    }
}
```
public/.htaccess
```htaccess
DirectoryIndex index.php
DirectorySlash Off

<IfModule mod_negotiation.c>
	Options -MultiViews
</IfModule>

<IfModule mod_rewrite.c>
	RewriteEngine On

	RewriteCond "%{REQUEST_URI}" "^(.*)/index.php$"
	RewriteRule ".*" "%1/" [R,L]

	<Files ".ht*">
		Require all granted
	</Files>
	RewriteCond "%{REQUEST_URI}" "\.ht[a-z]+$"
	RewriteRule "^" "/kernel.php" [END]

	RewriteCond "%{REQUEST_FILENAME}" "!-f"
	RewriteCond "%{REQUEST_FILENAME}index.php" "!-f"
	RewriteRule "^" "/kernel.php" [END]
</IfModule>
```

## Deployment
Actually, see [Quick Start](https://gitlab.com/Angelisium/octave/-/wikis/Quick-Start).
In the future there will be other main branches  (routing only, routing + custom view + custom model, routing + composer view + custom model, routing + custom view + composer ORM, routing + composer view + composer ORM, etc)... I think.

Minimal requirement :
 - PHP 8.0.x
 - Apache 2.4.x (or NGINX 1.21.x)

## File Structure

```
└── root/
	│
	├── public/				# Front-end stuff
	│	├── kernel.php		# Called each time no "static" controller is found
	│	└── .htaccess		# url rewriting for routing
	│	# teapot/ and index.php are examples of static controllers.
	│
	├── source/				# Back-end stuff
	│	├── controller/
	│	│	├── Blank.php	# empty router created to match with the regex
	│	│	│				# (normally should never be called since it is
	│	│	│				# duplicated with the static controller
	│	│	│				# public/index.php)
	│	│	└── SampleDynamicController.php # an example of dynamic controller
	│	│
	│	└── routes.regex	# The regex that manage dynamic routing.
	│
	├── AutoLoader.php		# Manages automatic loading (class, etc).
	└── CoreManager.php		# The core of all controllers
```

### various stuff to sort / rewrite / delete :

PHP Capitalization Syntaxe :
 - Class Name : UpperCamelCase
 - Method Name : camelCase
 - Property Name : snake_case
 - Variable Name : snake_case
 - Constant : SCREAMING_SNAKE_CASE
 - namespace : lowercase
 - parameter : camelCase

more soon...

To do:
 - clear the "view" section and make an example project with a "home made" view section and an example project with an composer view section (twig for eg)
 - idem for the "model" section, with a "home made" example and an composer example (doctrine for eg)
 - improve the HTTP response control features, the controller should be able to easily define the request header and body.
 - write documentation to explain the technical choices, and how to use the project easily and quickly.

 - add to doc : If u need composer stuff, use the composer autoloader with "composer dump-autoload" etc
