<?php
	use Core\CoreManager;

	class Controller extends CoreManager {
		public function GET(String $URI, String $ID):String {
			$this->ContentType = "application/json";

			return json_encode([
				'code' => $this->response['code'],
				'description' => $this->response['desc'],
				'page' => "Dynamic Controller",
				'slug' => $ID,
			], JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
		}
	}