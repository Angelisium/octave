<?php
	define('__ROOT__', dirname(__DIR__));
	require_once(__ROOT__ . "/AutoLoader.php");

	use Core\CoreManager;

	class Controller extends CoreManager {
		public function GET():String {
			$this->ContentType = "application/json";

			return json_encode([
				'code' => $this->response['code'],
				'description' => $this->response['desc'],
				'page' => "home"
			], JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
		}
	}

	$controller = new Controller();
	$controller->dispatch();