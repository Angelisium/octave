<?php
	define('__ROOT__', dirname(__DIR__, 2));
	require_once(__ROOT__ . "/AutoLoader.php");

	use Core\CoreManager;

	class Controller extends CoreManager {
		public function GET():String {
			$this->response['code'] = 418;
			$this->response['desc'] = "I’m a teapot";
			$this->ContentType = "application/json";

			return json_encode([
				'code' => 418,
				'description' => "I’m a teapot",
				'data' => []
			], JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
		}
		public function POST():String {
			return $this->GET();
		}
	}

	$controller = new Controller();
	$controller->dispatch();
